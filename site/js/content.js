var scriptPath = document.currentScript.src.split('?')[0], // remove any ?query
    scriptDirectory = scriptPath.split('/').slice(0, -1).join('/') + '/', // remove last filename part of path
    componentDirectory = scriptDirectory + "../",
    cssIncludes = ["css/content.min.css"],
    jsIncludes = ["js/libraries/popper/popper.min.js",
                  "js/libraries/tippy/tippy.min.js"
                 ],
    jsLoaded = 0,
    forLastExec,
    delay = 100,
    throttled = false;

if ((location.hostname === "localhost") || (location.hostname === "127.0.0.1")) {
    imageDirectory = "images/";
}

function loadCSS(file, location) {
    "use strict";

    var link = document.createElement("link");

    if (file.startsWith("http", 0) || file.startsWith("HTTP", 0)) {
        link.href = file;
    } else {
        link.href = componentDirectory + file;
    }

    link.type = "text/css";
    link.rel = "stylesheet";
    link.media = "screen, print";

    location.appendChild(link);
}

var loadJS = function (file, index, location) {
    "use strict";

    var script = document.createElement("script");

    script.id = "jsLoad" + index;
    script.type = "text/javascript";

    if (file.startsWith("http", 0) || file.startsWith("HTTP", 0)) {
        script.src = file;
    } else {
        script.src = componentDirectory + file;
    }

    script.async = false;
    location.appendChild(script);
    return script;
};

function setupHead() {
    "use strict";

    var hotspotSet,
        hotspotSetObject,
        backgroundElement;

    cssIncludes.forEach(function (item) {
        loadCSS(item, document.head);
    });
    
    // Add image placeholder to be added to hotspotSet div later
    for (hotspotSet in hotspotSets) {
        if (hotspotSets.hasOwnProperty(hotspotSet)) {
            hotspotSetObject = hotspotSets[hotspotSet];
            backgroundElement = document.createElement("img");
            backgroundElement.src = imageDirectory + hotspotSetObject.imageFileName;
            backgroundElement.alt = hotspotSetObject.imageAlt;
            hotspotSetObject.backgroundElement = backgroundElement;
        }
    }
}

function contentResize() {
    "use strict";
    var newWidth,
        hotspotSet,
        hotspotSetObject,
        hotspotSetElement,
        hotspot,
        hotspotObject,
        hotspotElement,
        backgroundElement,
        resizeRatio;

    // Resize all hotspot divs. Image resizes to hotspotSet container. Ratio is derived from original image width.
    for (hotspotSet in hotspotSets) {
        if (hotspotSets.hasOwnProperty(hotspotSet)) {
            hotspotSetObject = hotspotSets[hotspotSet];
            hotspotSetElement = hotspotSetObject.hotspotSetElement;
            backgroundElement = hotspotSetObject.backgroundElement;
            newWidth = (backgroundElement.offsetWidth);
            resizeRatio = newWidth / backgroundElement.naturalWidth;
            for (hotspot in hotspotSetObject.hotspots) {
                if (hotspotSetObject.hotspots.hasOwnProperty(hotspot)) {
                    hotspotObject = hotspotSetObject.hotspots[hotspot];
                    hotspotElement = hotspotObject.hotspotElement;
                    hotspotElement.style.left = (hotspotObject.originLeft * resizeRatio) + "px";
                    hotspotElement.style.top = (hotspotObject.originTop * resizeRatio) + "px";
                    hotspotElement.style.width = (hotspotObject.originWidth * resizeRatio) + "px";
                    hotspotElement.style.height = (hotspotObject.originHeight * resizeRatio) + "px";
                }
            }
        }
    }
}

function setupBody() {
    "use strict";

    var hotspotSet,
        hotspotSetObject,
        hotspotSetElement,
        hotspot,
        hotspotObject,
        hotspotElement;
    
    // Add background image to hotspotSet div. Add hotspot element divs from information in hotspotSets object array. 
    for (hotspotSet in hotspotSets) {
        if (hotspotSets.hasOwnProperty(hotspotSet)) {
            hotspotSetObject = hotspotSets[hotspotSet];
            hotspotSetElement = document.getElementById(String(hotspotSet));
            hotspotSetElement.appendChild(hotspotSetObject.backgroundElement);
            hotspotSetObject.hotspotSetElement = hotspotSetElement;
            hotspotSetObject.borderWidth = window.getComputedStyle(hotspotSetElement).borderLeftWidth.slice(0, -2);
            for (hotspot in hotspotSetObject.hotspots) {
                if (hotspotSetObject.hotspots.hasOwnProperty(hotspot)) {
                    hotspotObject = hotspotSetObject.hotspots[hotspot];
                    hotspotElement = document.createElement("button");
                    hotspotElement.type = "button";
                    hotspotObject.hotspotElement = hotspotElement;

                    hotspotObject.originLeft = hotspotObject.left;
                    hotspotElement.style.left = hotspotObject.left + "px";

                    hotspotObject.originTop = hotspotObject.top;
                    hotspotElement.style.top = hotspotObject.top + "px";

                    hotspotObject.originWidth = hotspotObject.width;
                    hotspotElement.style.width = hotspotObject.width + "px";

                    hotspotObject.originHeight = hotspotObject.height;
                    hotspotElement.style.height = hotspotObject.height + "px";

                    hotspotElement.classList.add("hotspot");
                    hotspotElement.classList.add("outline");
                    hotspotElement.id = hotspot;

                    hotspotElement.setAttribute("data-tippy-content", hotspotObject.content);
                    hotspotSetElement.appendChild(hotspotElement);
                }
            }
        }
    }

    // Add the JS includes at the end of the body and set up tippy functionality after all includes have been loaded
    jsIncludes.forEach(function (item, index) {
        loadJS(item, index, document.body).addEventListener('load', function () {
            jsLoaded += 1;
            if (jsLoaded === jsIncludes.length) {
                for (hotspotSet in hotspotSets) {
                    if (hotspotSets.hasOwnProperty(hotspotSet)) {
                        hotspotSetObject = hotspotSets[hotspotSet];
                        tippy("#" + String(hotspotSet) + " .hotspot", {
                            theme: "contrast",
                            arrow: true,
                            animation: "fade",
                            boundary: hotspotSetObject.backgroundElement,
                            flipOnUpdate: false,
                            hideOnClick: false
                        });
                    }
                }
                
                contentResize();
            }
        });
    });
}

setupHead();

window.addEventListener("resize", function () {
    "use strict";
    if (!throttled) {
        contentResize();
        throttled = true;

        setTimeout(function () {
            throttled = false;
        }, delay);
    }
    clearTimeout(forLastExec);
    forLastExec = setTimeout(contentResize, delay);
});

window.onload = setupBody;
