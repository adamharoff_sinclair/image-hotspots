image-hotspots component

Use index.html as a template: https://flashmedia.sinclair.edu/dl/DEPT/eCDD/components/image-hotspots/index.html

Variables used within the [<script>] tags at the top of the page's [<head>] section (* = Required):
    - imageDirectory* -  absolute path to the folder for the hotspotSets on this page
    - hotspotsSets* - name of the object that holds all the hotspotSets for this page. It is preferable to use the naming convention hotspotSet + number sequence (hotspotSet1, hotspotSet2, etc). These names will be referenced in the [<body>] that holds the set. 

    Each hotspotSet must:
        - be placed in an individual div
        - have the class hotspotSet
        - use the id name given in the hotspotSets object

    The hotspotSet object contains the following:
        - imageFileName* - the file name of the image for this hotspotSet. This name must include the file extension. The file must reside in the imageDirectory folder, though sub-folders can be included in imageFileName.
        - imageAlt* - the alternate text that will be added to the image for accessibility
        - persistentLabels - (optional) set to true for labels that do not disappear 
        - hotspots* - this object holds all the hotspots for this hotspotSet. Each hotspot should use the naming convention hotspot + number sequence (hotspot1, hotspot2, etc). Each hotspot object contains the following:
            - left* - Distance, in pixels, that the hotspot will be positioned from the left of the image. Can be thought of as the x-position.
            - top* - Distance, in pixels, that the hotspot will be positioned from the top of the image. Can be thought of as the y-position.
            - width* - Width of the hotspot in pixels
            - height* - Height of the hotspot in pixels
            - content* - The text that will show when the hotspot is rolled over
            - placement - (optional) go to https://atomiks.github.io/tippyjs/v6/all-props/#placement for a full list


Restrictions
(See the "Future updates" section for instructions to submit update suggestions)
    - All hotspots are blue outlines with a black rollover with a consistent outline width of 6px. The outline stays this width no matter how small the image gets.
    - All info boxes are white text on a black background and inherit the font family from the page content. font-size is set at 1em.
    - All hotspotSets have a 10px grey border around the image.
    - The hotspotSet expands until it reaches the width of the original image. This prevents the image from getting too large and distorted. When using this component in D2L, the maximum size should be the width of the D2L content frame:
        - Retina resolution: 2340px container - 20px border = 2320px image
        - Standard resolution: 1170px container - 12px border = 1150px image
        - The image can be as tall as necessary


Features
    - Responsive
    - Accessible
    - Each hotspotSet can be placed among other content already present on a D2L page
    - There can be several hotspotSets per page.
    - Utilizes a single code base. No need to include the file package in every course that uses this component. This will make it easier to fix issues and add new features. Please keep track of usage.


Future updates
    - Submit update suggestions to adam.haroff@sinclair.edu with a summary of the preferred functionality and reasons why it would be an improvement.


Issues
    - Submit issues to adam.haroff@sinclair.edu including the following:
        - Observed behavior, with screenshot if relevant
        - Expected behavior
        - Steps to reproduce
        - Browser, including the version number
        - Operating system, including the version number